// Дан массив с элементами [1, 2, 3, 4, 5]. С помощью цикла for выведите все эти
// элементы на экран.
let arr=[1,2,3,4,5];

for(let i=0; i<arr.length;i++){
    document.write(arr[i])
}

// Дан массив с числами [-2, -1, -3, 15, 0, -4, 2, -5, 9, -15, 0, 4, 5, -6, 10, 7]. Числа могут
// быть положительными и отрицательными. Выведите на экран только отрицательные
// числа, которые больше -10, но меньше -3.
let  arrPosit=[-2, -1, -3, 15, 0, -4, 2, -5, 9, -15, 0, 4, 5, -6, 10, 7];
let newArrPosit=[]
for(let i=0; i<arrPosit.length;i++){
   
    if(arrPosit[i]>-10 && arrPosit[i]<-3){
       newArrPosit.push(arrPosit[i])
    }
}

document.write(`<br> Задание №2:  ${newArrPosit}`)

// Создайте новый массив и заполните его значениями от 23 до 57, используя цикл for и
// while. Выведите оба массива. С помощью цикла for найдите сумму элементов этого
// массива. Запишите ее в переменную result и выведите.
arrNew=[]
for( let i=27; i<=57;i++){
  arrNew.push(i)
}
console.log(arrNew);
let result=0;
for( let i=0; i<arrNew.length;i++){
//   console.log(arrNew[i]);
result+=arrNew[i];

}
console.log(result);

let w=27;
while( w<57){
    w++
    arrNew.push(w)
}
console.log(arrNew);
// 4. Дан массив числами (строчного типа), например: [‘10’, ‘20’, ‘30’, ‘50’, ‘235’, ‘3000’].
// Выведите на экран только те числа из массива, которые начинаются на цифру 1, 2 или
// 5.
let arrStr=['10', '20', '30', '50', '235', '3000']
for (let i = 0; i < arrStr.length; i++) {
    let num = String(arrStr[i]);
   
    let char = num[0];
    // console.log(char);
    if (char == 1 || char == 2 || char == 5) {
        document.write(`<br> ${num}`)
    }
  }
//   5. Составьте массив дней недели (ПН, ВТ, СР и т.д.). С помощью цикла for выведите все
// дни недели, а выходные дни выведите жирным.

let week=['Monday','Tuesday', 'Wednesday','Thursday','Friday','Saturday','Sunday']
for(let i=0;i<week.length;i++){
   if(week[i] == 'Saturday' || week[i] == 'Sunday' ){
    document.write(`<br> <b> ${week[i]}</b>`)
    
    
} else {
    document.write(`<br>  ${week[i]}`)
}
} 
// 6. Создайте массив с произвольными данными. Добавьте в конец массива любой элемент,
// и получите последний элемент массива, используя свойство length.

let arrAny=[1,2,-5,-6454,12556]
arrAny.push('Element')
console.log(arrAny);
// console.log(arrAny.length-1);
const lastItem = arrAny.length -1;
console.log("Последний элемент массива:",arrAny[lastItem]);
// 7. Запросите у пользователя по очереди числовые значения при помощи prompt и
// сохраните их в массив. Собирайте числа до тез пор пока пользователь не введет пустое
// значение. Выведите получившийся массив на экран. Выполните сортировку чисел
// массива, и выведите его на экран.
let arrNumber=[]


for(;;){
  const number =+prompt('введите число')
  if(number == ' ') {
    break;
  }
  if(isNaN(number)===true){
    alert(`Ошибка`);
    break;
  }
  // document.write(number)
  arrNumber.push(number)
  // console.log(arrNumber);
  arrNumber.sort(function (a,b){
    return a-b;

  });
 
}

document.write(`<br> ${arrNumber}`)
// 8. Переверните массив [12, false, ‘Текст’, 4, 2, -5, 0] (выведите в обратном порядке),
// используя цикл while и метод reverse.
let arrRev=[12, false, 'Текст', 4, 2, -5, 0];
arrRev.reverse();
console.log(arrRev);
let arrNewReverse='';
let rev;
// while(rev=arrRev.length-1){
//   rev>=0;
//   rev--;
//   arrNewReverse+=arrRev[rev];
//   console.log(arrNewReverse);
// }



// 9. Напишите скрипт, считающий количество нулевых (пустых) элементов в заданном
// целочисленном массиве [5, 9, 21, , , 9, 78, , , , 6].
let count
let arrEmpty=[5, 9, 21, , , 9, 78, , , , 6]
for(i=0;i<arrEmpty.length;i++){

}  
// console.log(arrEmpty.length);


let filterArrEmty=arrEmpty.filter(Boolean)
// console.log(filterArrEmty)
for(i=0;i<filterArrEmty.length;i++){

}
// console.log(filterArrEmty.length);
count=arrEmpty.length-filterArrEmty.length;
console.log(`${count} пустых строк`);

// 10. Найдите сумму элементов массива между двумя нулями (первым и последним нулями
//   в массиве). Если двух нулей нет в массиве, то выведите ноль. В массиве может быть
//   более 2х нулей. Пример массива: [48,9,0,4,21,2,1,0,8,84,76,8,4,13,2] или
//   [1,8,0,13,76,8,7,0,22,0,2,3,2].
let arrFind=[1,8,0,13,76,8,7,0,22,0,2,3,2]
let first=arrFind.indexOf(0);
console.log(first);
 
let last=arrFind.lastIndexOf(0)
console.log(last);
let sum=0
for (i=0;i<arrFind.length;i++){
    // console.log(i);
   
 
    if(i>first && i<last){
        sum+=arrFind[i]
    }
}console.log(sum);

// 11. *** Нарисовать равнобедренный треугольник из символов ^. Высоту выбирает
// пользователь. Например: высота = 5, на экране:
let n = "&nbsp"
let triangleNumber=prompt(`Задайте высоту треугольника`)

for (i = 0; i < triangleNumber; i++) {
  for (let j = triangleNumber; j > i - 1; j--) {
    document.write(n, n)
  }
  for (let j = 0; j < i + 1; j++) {
    document.write('^', n, n)
  }
  document.write('<br>')
}